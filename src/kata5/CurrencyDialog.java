/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kata5;

import java.awt.Component;
import javax.swing.JComboBox;
import javax.swing.JPanel;


public class CurrencyDialog extends JPanel {

    public CurrencyDialog() {
        add(CreateComboBox());
    }

    private Component CreateComboBox() {
        return new JComboBox(new String[] {"EUR","USD", "CHF", "GBP"});
    }
    
    
}
