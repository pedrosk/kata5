package kata5;

import java.awt.FlowLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MoneyDialog extends JPanel {

    public MoneyDialog() {
        super(new FlowLayout());
        add(createTextEdit());
        add(new CurrencyDialog());
    }

    private JComponent createTextEdit() {
        JTextField textfield = new JTextField(15);
        
        return textfield;
    }
}
